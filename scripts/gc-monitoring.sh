#!/bin/bash

hostname=$(hostname)
#Calculate the usage
awk -v d1="$(date --date="-5 min" "+%b %_d %H:%M")" -v d2="$(date "+%b %_d %H:%M")" '$0 > d1 && $0 < d2 || $0 ~ d2' $TOMCAT_HOME/log/gc.log | grep -i "Allocation Failure" > /tmp/gc_fail.txt

count=0
while read p; do
y=$(echo $p | head -n1 | awk '{print $7;}' | sed 's/M->.*//');
x=$(echo $p | head -n1 | awk '{print $7;}' | sed 's/^[0-9]*M->//' | sed 's/M(.*//');
z=$(printf '%.2f\n' $(echo "$x/$y" | bc -l))
if (( $(echo "$z > 0.8" |bc -l) )); then
   count=$((count+1))
fi
done < /tmp/gc_fail.txt

#Calculate percantage of pause time
awk -v d1="$(date --date="-5 min" "+%b %_d %H:%M")" -v d2="$(date "+%b %_d %H:%M")" '$0 > d1 && $0 < d2 || $0 ~ d2' test | grep -i "GC pause" > /tmp/gc_pause.txt
sum=0
while read p; do
t=$(echo $p | head -n1 | sed 's/ secs.*//' | awk '{print $NF}');
sum=$(printf '%.5f\n' $(echo "$sum+$t" | bc -l))
done < /tmp/gc_pause.txt
pause=$(printf '%.5f\n' $(echo "$sum/300" | bc -l))

# Check Pause time percantage and Usage
if [ $count -gt 10 ] || [ $pause -gt 0.15 ]; then
  srv=0
  for ip in 10.15.{1..4).10
  do
    if curl --output /dev/null --silent --head --fail "http://$ip:8080"; then
      srv=$((srv+1))
    fi
  done
fi

# Check other nodes and Restart Tomcat service on the instance
if [[ $srv -gt 2 ]]; then
    service tomcat stop
    kill -9 $(ps -ef | grep tomcat | grep -v "grep" | awk '{print $2}')
    service tomcat start
    gzip -c ~/event.json | curl --data-binary @- -X POST -H "Content-Type: application/json" -H "X-Insert-Key: b4f8a81d7c226fdf5f51fd9371e8f9220e10713b" -H "Content-Encoding: gzip" https://insights-collector.newrelic.com/v1/accounts/2427181/events
fi

sleep 5
if
  srv=$((srv+1))
fi

if curl --output /dev/null --silent --head --fail "http://localhost:8080"; then
  echo "Tomcat is up"
else
  gzip -c ~/event.json | curl --data-binary @- -X POST -H "Content-Type: application/json" -H "X-Insert-Key: b4f8a81d7c226fdf5f51fd9371e8f9220e10713b" -H "Content-Encoding: gzip" https://insights-collector.newrelic.com/v1/accounts/2427181/events
fi
