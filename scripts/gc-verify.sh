#!/bin/bash

### Verify garbage collection logging for Apache Tomcat is enabled
if [ ! -f $TOMCAT_HOME/bin/setenv.sh ]; then
  cat >> $TOMCAT_HOME/bin/setenv.sh <<EOL
  export CATALINA_OPTS=" \

  -XX:+PrintGCTimeStamps \
  -XX:+PrintGCDetails \
  -XX:+PrintGCApplicationStoppedTime \
  -XX:+PrintGCApplicationConcurrentTime \
  -XX:+PrintHeapAtGC \
  -Xloggc:logs/gc.log"
EOL
chmod 775 $TOMCAT_HOME/bin/setenv.sh
fi

### Verify rotation of garbage collection logging for Apache Tomcat is enabled
if [ ! -f /etc/logrotate.d/gc ]; then
  cat >> /etc/logrotate.d/gc <<EOL
  $TOMCAT_HOME/log/gc.log {
         daily
         rotate 7
         delaycompress
         compress
         notifempty
         missingok
  }
EOL
logrotate -df -s /etc/logrotate.d/gc
fi
