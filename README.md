# Alica Tomcat Application Assignment

## To monitor events/logs of Tomcat server, we need to add two script in cron of 4 servers as below:
- `gc-verify.sh` run on daily basis once at 00:00 to verify GC is enabled and old logs rotated.
- `gc-monitoring.sh` run every 5 min to:
    1. Count the number of FullGC events in which the memory being freed is below 20% during a 5 min window. If count > 10.
    2. If total FullGC time during a 5min window is greater than 15%.
    3. Check Tomcat and website is responsive on two servers before restart it locally.
    4. Restart Tomcat if three conditions above happened.
    5. Send Alert through NewRelic.
    6. Verify Tomcat is up and running after restart.

## Send Alert through NewRelic:
  - Install the Node.js agent `apt/yum install npm -y` && `npm install newrelic`
  - `cp node_modules/newrelic/newrelic.js $TOMCAT_HOME/newrelic.js`
      `newrelic.js` in `alice-assignment` repo has **license_key**
  - Make `script require('newrelic');` the first line of your Tomcat startup
    example: file `app.js`: line 1 -> `var nr = require('newrelic');`
  - (re)deploy App.
  - copy `event.json` in user home directory.
